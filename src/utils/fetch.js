import axios from 'axios';
import querystring from 'querystring'

axios.defaults.headers.common['Content-Type'] = 'application/json';
axios.defaults.baseURL = process.env.REACT_APP_API_URL;

export function doGet(url, queryParams) {
    return axios.get(url + (queryParams ?  `?${querystring.encode(queryParams)}` : ''));
}

export function doPatch(url, body) {
    return axios.patch(url, body);
}

export function doPost (url, body) {
    return axios.post(url, body);
}

export function doDelete (url, queryParams) {
    return axios.delete(url, queryParams)
}