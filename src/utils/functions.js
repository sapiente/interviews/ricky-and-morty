import {List, Record} from "immutable";

export function createModel(name, fromJson) {
    const Class = Record(fromJson({}), name);
    Class.fromServer = (json = {}) => new Class(fromJson(json));
    Class.fromServerList = (json = []) => List(json.map(Class.fromServer));

    return Class;
}

export const range = (start, end) => (end - start) > 0 ? [...Array(end - start).keys()].map(x => x + start) : []
export const getIdFromLastSegmentOfUrl = (url) => url.split('/').pop();
