import {doGet} from "../utils/fetch";
import Character from "../models/Character";
import PaginationInfo from "../models/PaginationInfo";
import {getIdFromLastSegmentOfUrl} from "../utils/functions";

export async function getCharactersByPage(page) {
    const {data} = await doGet('/api/character/', {page});
    data.results.forEach((result) => {
        result.location.id = getIdFromLastSegmentOfUrl(result.location.url)
    })

    return {
        characters: Character.fromServerList(data.results),
        paginationInfo: PaginationInfo.fromServer(data.info).set('currentPage', page)
    };
}

export async function getCharactersByIds(ids) {
    const {data} = await doGet(`/api/character/${ids.join(',')},`);
    return Character.fromServerList(data);
}