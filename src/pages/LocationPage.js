import React, {useEffect, useState} from 'react';
import Location from "../models/Location";
import Spinner from "../components/Spinner";
import UniverseIcon from "../components/icons/UniverseIcon";
import {faUsers} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {getLocationDetail} from "../api/locationsApi";
import PlanetIcon from "../components/icons/PlanetIcon";

export default function LocationPage(props) {
    const [location, setLocation] = useState(Location())
    const [loading, setLoading] = useState(false)

    useEffect(() => {
        const fetchLocation = async () => {
            setLoading(true);
            setLocation (await getLocationDetail(props.match.params.id));
            setLoading(false);
        };

        fetchLocation();
    }, [props.match.params.id]);

    if (loading) {
        return <Spinner/>
    }

    return (
        <div className="container">
            <div className="location-detail">
                <PlanetIcon/>
                <h1>{location.name}</h1>
                <UniverseIcon />
                <div className="location-detail-universe">{location.dimension}</div>
                <FontAwesomeIcon icon={faUsers} />
                <div className="location-detail-residents">
                    {location.residents.map(resident => <span>{resident.name}</span>)}
                </div>
            </div>
        </div>
    );
}