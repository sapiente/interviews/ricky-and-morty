import React from 'react';

import '../assets/sass/main.scss';
import Header from "./Header";
import {BrowserRouter as Router, Switch, Route, Redirect,} from "react-router-dom";

import PageNotFound from "../pages/PageNotFound";
import CharactersPage from "../pages/CharactersPage";
import LocationsPage from "../pages/LocationsPage";
import LocationPage from "../pages/LocationPage";

function App() {
    return (
            <Router basename={process.env.REACT_APP_BASE_PATH}>
                <Header/>
                <main>
                    <Switch>
                        <Redirect exact path="/" to="/characters"/>
                        <Route exact path="/characters" component={CharactersPage}/>
                        <Route exact path="/locations" component={LocationsPage}/>
                        <Route exact path="/locations/:id" component={LocationPage}/>
                        <Route component={PageNotFound}/>
                    </Switch>
                </main>
            </Router>
    );
}

export default App;
