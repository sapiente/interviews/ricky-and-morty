import React from 'react';
import {range} from "../utils/functions";

const Pagination = ({paginationInfo, paginate }) => {
    const pages = range(1, paginationInfo.pagesCount);

    return (
        <nav>
            <ul className='pagination'>
                {pages.map((pageNumber) => (
                    <li key={pageNumber} className='page-item'>
                        {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
                        <a onClick={() => paginate(pageNumber)} href='#' className={"page-link" + (paginationInfo.currentPage === pageNumber ? ' page-link-active' : '')}>
                            {pageNumber}
                        </a>
                    </li>
                ))}
            </ul>
        </nav>
    );
};

export default Pagination;