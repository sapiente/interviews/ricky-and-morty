import React, {useState} from 'react';

export default function ImageWithPlaceholder({src, placeholder}) {
    const [loaded, setLoaded] = useState(false);

    return (
        <>
            {!loaded ? (
                <img alt="" src={placeholder} />
            ) : null}
            <img alt="" src={src} style={!loaded ? {display: 'none'} : {}} onLoad={() => setLoaded(true)} />
        </>
    );
}