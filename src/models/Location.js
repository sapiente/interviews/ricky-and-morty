import {createModel} from "../utils/functions";
import {List} from "immutable";
import Character from "./Character";

const Location = createModel('Location', (json) => ({
    id: json.id,
    name: json.name,
    dimension: json.dimension,
    residents: List((json.residents || []).map(Character))
}));


export default Location;