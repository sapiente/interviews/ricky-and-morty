import {createModel} from "../utils/functions";
import {Record} from "immutable";

const Location = Record({id: null, name: null});

const Character = createModel('Character', (json) => ({
    id: json.id,
    name: json.name,
    species: json.species,
    image: json.image,
    status: json.status,
    location: Location({
        id: (json.location && json.location.id) || null,
        name: (json.location && json.location.name) || null,
    })
}));

export default Character;