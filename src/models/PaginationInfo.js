import {createModel} from "../utils/functions";

const PaginationInfo = createModel('PaginationInfo', (json) => ({
    pagesCount: json.pages,
    currentPage: null
}));

export default PaginationInfo;